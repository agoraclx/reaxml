<?php
/**
 * Created by PhpStorm.
 * User: AGORACLX
 * Date: 10/23/2018
 * Time: 1:18 PM
 */

namespace REA;


class Features
{

    protected $bedrooms;
    protected $bathrooms;
    protected $garages;
    protected $carports;
    protected $airConditioning;
    protected $pool;
    protected $alarmSystem;
    protected $otherFeatures;


    /**
     * @return mixed
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }


    /**
     * @param mixed $bedrooms
     */
    public function setBedrooms($bedrooms)
    {
        $this->bedrooms = $bedrooms;
    }

    /**
     * @return mixed
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }


    /**
     * @param mixed $bathrooms
     */
    public function setBathrooms($bathrooms)
    {
        $this->bathrooms = $bathrooms;
    }

    /**
     * @return mixed
     */
    public function getGarages()
    {
        return $this->garages;
    }

    /**
     * @param mixed $garages
     */
    public function setGarages($garages)
    {
        $this->garages = $garages;
    }


    /**
     * @return mixed
     */
    public function getCarports()
    {
        return $this->carports;
    }

    /**
     * @param mixed $carports
     */
    public function setCarports($carports)
    {
        $this->carports = $carports;
    }


    /**
     * @return mixed
     */
    public function getAirConditioning()
    {
        return $this->airConditioning;
    }

    /**
     * @param mixed $airConditioning
     */
    public function setAirConditioning($airConditioning)
    {
        $this->airConditioning = $airConditioning;
    }

    /**
     * @return mixed
     */
    public function getPool()
    {
        return $this->pool;
    }

    /**
     * @param mixed $pool
     */
    public function setPool($pool)
    {
        $this->pool = $pool;
    }

    /**
     * @return mixed
     */
    public function getAlarmSystem()
    {
        return $this->alarmSystem;
    }

    /**
     * @param mixed $alarmSystem
     */
    public function setAlarmSystem($alarmSystem)
    {
        $this->alarmSystem = $alarmSystem;
    }

    /**
     * @return mixed
     */
    public function getOtherFeatures()
    {
        return $this->otherFeatures;
    }

    /**
     * @param mixed $otherFeatures
     */
    public function setOtherFeatures($otherFeatures)
    {
        $this->otherFeatures = $otherFeatures;
    }



}