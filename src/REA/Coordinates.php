<?php
/**
 * Created by PhpStorm.
 * User: AGORACLX
 * Date: 10/9/2018
 * Time: 2:49 PM
 */

namespace REA;


class Coordinates
{
    protected $latitude;
    protected $longitude;


    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    public function getLatidude()
    {
        return $this->latitude;
    }


    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }




}